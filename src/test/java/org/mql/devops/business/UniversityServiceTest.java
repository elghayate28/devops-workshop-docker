package org.mql.devops.business;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;
import org.mql.devops.dao.StudentDao;
import org.mql.devops.models.Student;

public class UniversityServiceTest {
    private UniversityService universityService;
    private StudentDao studentDao;

    @BeforeEach
    void setup() {
        studentDao = Mockito.mock(StudentDao.class);
        universityService = new UniversityServiceDefault(studentDao);
    }

    @Test
    void testFindById(){
        Mockito.when(studentDao.findById("1")).thenReturn(new Student("1", "Erich", "Gamma", "JavaScript"));
        Student student = universityService.student("1");
        assertEquals("1",student.getCnie());
        assertEquals("Erich",student.getFirstname());
    }
}

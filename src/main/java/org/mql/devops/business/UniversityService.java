package org.mql.devops.business;

import java.util.List;

import org.mql.devops.models.Student;

public interface UniversityService {
	public List<Student> students();
	public void insertStudent(Student student);
	public Student deleteStudentByCnie(String cnie);
	public Student student(String cnie);
}
